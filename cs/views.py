from .models import Media
from .forms import MediaForm
from django.shortcuts import redirect, render
from django.http.response import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required

#@login_required(login_url='/admin/login/')

def index(request):
    """
    form = MediaForm(request.POST or None)
    
    if form.is_valid() and request.method == 'POST':
        form.save()
        return redirect('hubungi')
    
    response = { 'form' : form }
    """

    return render(request, 'cs_index.html')

def tambah(request):
    if request.method == 'POST':
        nama = request.POST['nama']
        media = request.POST['media']

    Media.objects.create(
        nama = nama,
        media = media
    )
    return HttpResponse('')
